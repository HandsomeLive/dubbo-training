package top.cmagic.service.intf;

import top.cmagic.web.entity.User;

import java.util.List;

/**
 * Created by 30804 on 2017/11/29.
 */
public interface DemoService {
    List<User> getDemoService(String username);
}
