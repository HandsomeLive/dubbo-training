//package top.cmagic.config;
//
//import org.apache.dubbo.config.ApplicationConfig;
//import org.apache.dubbo.config.ConsumerConfig;
//import org.apache.dubbo.config.RegistryConfig;
//import org.apache.dubbo.config.spring.context.annotation.DubboComponentScan;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
///**
// * @Description:
// * @Author: WangShiCheng
// * @Date: Created on 2017/12/31.
// */
//@Configuration
//@DubboComponentScan(basePackages = "top.cmagic")
//public class DubboControllerConfig
//{
//
//    @Bean
//    public ApplicationConfig applicationConfig(){
//        ApplicationConfig applicationConfig=new ApplicationConfig();
//        applicationConfig.setName("demo-consumer");
//        return applicationConfig;
//    }
//
//    @Bean
//    public ConsumerConfig consumerConfig(){
//        ConsumerConfig consumerConfig=new ConsumerConfig();
//        consumerConfig.setTimeout(5000);
//        consumerConfig.setCheck(false);
//        return consumerConfig;
//    }
//
//    @Bean
//    public RegistryConfig registryConfig(){
//        RegistryConfig registryConfig=new RegistryConfig();
//        registryConfig.setAddress("zookeeper://192.168.31.226:2181");
//        registryConfig.setCheck(false);
//        return registryConfig;
//    }
//
//}
