package top.cmagic.controller;

import org.apache.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import top.cmagic.service.intf.DemoService;
import top.cmagic.web.entity.User;

import java.util.List;

/**
 * Created by 30804 on 2017/12/2.
 */
@Controller
@RequestMapping("/user")
public class UserController {
    @Reference
    private DemoService demoService;

    @RequestMapping("/login.do")
    @ResponseBody
    public List<User> userLogin(){
        return demoService.getDemoService("zhangsan");
    }
}
