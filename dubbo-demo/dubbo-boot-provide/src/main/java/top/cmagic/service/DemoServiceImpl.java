package top.cmagic.service;

import org.apache.dubbo.config.annotation.Service;
import top.cmagic.service.intf.DemoService;
import top.cmagic.web.entity.User;

import java.util.ArrayList;
import java.util.List;

/**
 * @author wsc
 * @date: 2019年07月19日 17:28
 * @since JDK 1.8
 */
@Service
public class DemoServiceImpl implements DemoService {
    @Override
    public List<User> getDemoService(String username) {
        List<User> users=new ArrayList<>();
        User user=new User();
        user.setUserName("zhangsan");
        user.setPassword("123456");
        users.add(user);
        return users;
    }
}
