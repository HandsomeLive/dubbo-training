package top.cmagic;

import com.alibaba.fastjson.JSON;
import org.apache.zookeeper.*;

import java.io.IOException;
import java.util.concurrent.CountDownLatch;

/**
 * @author wsc
 * @date: 2019年07月21日 12:24
 * @since JDK 1.8
 */
public class ZookeeperDemo {

    public static void main(String[] args) throws IOException, InterruptedException, KeeperException {
        final CountDownLatch countDownLatch =new CountDownLatch(1);
        ZooKeeper zooKeeper=new ZooKeeper("192.168.31.228:2181", 5000, new Watcher() {
            public void process(WatchedEvent watchedEvent) {
                System.out.println("watchedEvent: "+ JSON.toJSONString(watchedEvent));
                Event.KeeperState state = watchedEvent.getState();
                Event.EventType type = watchedEvent.getType();
                if (Event.KeeperState.SyncConnected==state){
                    if (Event.EventType.None==type){
                        System.out.println("连接成功");
                        countDownLatch.countDown();
                    }
                }
            }
        });
        countDownLatch.await();
        Thread.sleep(1000);
//        zooKeeper.create("/demo", "1234".getBytes("utf-8"), ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);
        zooKeeper.exists("/demo", true);
        zooKeeper.delete("/demo",-1);
        System.out.println("1111111111111");
        zooKeeper.close();
    }

}
